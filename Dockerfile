FROM openjdk:17-jdk-alpine
WORKDIR /app
COPY /target/simple-maven-app-0.0.1-SNAPSHOT.jar /app/maven-app.jar

ENTRYPOINT ["java","-cp","maven-app.jar","ma.uiass.eia.maven.App"]
